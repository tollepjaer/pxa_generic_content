# // Default Fluid Content
mod.wizards.newContentElement.wizardItems.pxa-generic-content-templates {
 	# Set header here so it becomes translated
    header = LLL:EXT:pxa_generic_content/Resources/Private/Language/locallang.xlf:pxa-generic-content-templates.header
}

mod.wizards.newContentElement.wizardItems.pxa-generic-content-layouts {
    # Set header here so it becomes translated
    header = LLL:EXT:pxa_generic_content/Resources/Private/Language/locallang.xlf:pxa-generic-content-layouts.header
}

mod.wizards.newContentElement.wizardItems.pxa-generic-content-special {
    # Set header here so it becomes translated
    header = LLL:EXT:pxa_generic_content/Resources/Private/Language/locallang.xlf:pxa-generic-content-special.header
}

# # Remove some fluidcontent for non admins
[globalVar = BE_USER|user|admin = 0]

mod.wizards.newContentElement.wizardItems.pxa-generic-content-templates {
    show:= removeFromList(pxa_generic_content_Contact_html)
    show:= removeFromList(pxa_generic_content_Logo_html)
    show:= removeFromList(pxa_generic_content_SocialIcons_html)
}

mod.wizards.newContentElement.wizardItems.pxa-generic-content-layouts {
    show:= removeFromList(pxa_generic_content_AdvGrid_html)
    show:= removeFromList(pxa_generic_content_Grid_html)
}
mod.wizards.newContentElement.wizardItems.pxa-generic-content-special {
    show:= removeFromList(pxa_generic_content_MainNavToggleButton_html)
    show:= removeFromList(pxa_generic_content_SubNavigation_html)
    show:= removeFromList(pxa_generic_content_SearchToggleButton_html)
    show:= removeFromList(pxa_generic_content_Breadcrumbs_html)
    show:= removeFromList(pxa_generic_content_SearchForm_html)
    show:= removeFromList(pxa_generic_content_MainNavigation_html)
    show:= removeFromList(pxa_generic_content_LanguageMenu_html)

}
# Try to clear the complete tab
mod.wizards.newContentElement.wizardItems.pxa-generic-content-special >

[GLOBAL]

# mod.web_list {  
# 	# Set so new content displays wizard
# 	newWizards = 1

# 	# Disable add content in list mode
# 	#deniedNewTables = tt_content
# }

