[GLOBAL]
plugin.tx_pxagenericcontent.view {
    # label = Fluid Content Elements - Twitter Bootstrap
    # extensionKey = FluidTYPO3.FluidcontentBootstrap
    templateRootPath = EXT:pxa_generic_content/Resources/Private/Templates/
    partialRootPath = EXT:flux/Resources/Private/Partials/
    layoutRootPath = EXT:fluidcontent/Resources/Private/Layouts/
}
plugin.tx_fluidcontent.view {
    templateRootPath = EXT:pxa_generic_content/Resources/Private/Templates/
}

    // Settings for iconSet
plugin.tx_pxagenericcontent {
    settings {
        image {
            maxWidth = {$styles.content.imgtext.maxW}
        }
        iconSet {
            fontSize = 30px
            columns = 10
            sourceFolder =
            fileNameJson = selection.json
            fileNameCss = style.css
        }
        aloha {
            enable = 0
            header {
                tag=div
                allow=edit
                class=heading
            }
            plaintext {
                tag=p
                allow=edit
                class=alohaeditable-plaintext
            }
            plaintextSpan {
                tag=span
                allow=edit
                class=alohaeditable-plaintext
            }
            rte {
                tag=span
                allow=edit
                class=alohaeditable-block
            }
            input {
                tag=div
                allow = edit
                class=nostyles
            }
        }
        content {
            LanguageMenu {
                defaultLanguageLabel = {$tx_pxagenericcontent.config.defaultLanguageLabel}
                defaultIsoFlag = {$tx_pxagenericcontent.config.defaultIsoFlag}
                hideNotTranslated = {$tx_pxagenericcontent.config.hideNotTranslated}
            }
        }
    }
}

    // Remove default header
tt_content.fluidcontent_content.10 >
